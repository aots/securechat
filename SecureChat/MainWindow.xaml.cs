﻿using LiteNetLib;
using MahApps.Metro.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;

namespace SecureChat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow: INotifyPropertyChanged
    {
        ObservableCollection<MetroTabItem> TabsList { get; set; } = new ObservableCollection<MetroTabItem>();
        public MainWindow()
        {
            InitializeComponent();

            Title = $"Secure Chat {FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion}";
            AddChatTabButton.Click += AddChatTabButton_Click;
            TabControl.ItemsSource = TabsList;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void AddChatTabButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var chat = new ChatWindow("Key here");
            var tab = new MetroTabItem
            {
                Header = "SHIT",
                Content = chat.Content
            };
            TabsList.Add(tab);
        }
    }
}