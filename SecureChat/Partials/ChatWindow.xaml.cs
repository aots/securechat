﻿using LiteNetLib;
using LiteNetLib.Utils;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;

namespace SecureChat
{
    /// <summary>
    /// Interaction logic for ChatWindow.xaml
    /// </summary>
    public partial class ChatWindow : UserControl, INotifyPropertyChanged
    {
        private string _chatText = "";
        private string _inputText = "";

        public event PropertyChangedEventHandler PropertyChanged;

        private EventBasedNetListener Listener { get; set; } = new EventBasedNetListener();
        private NetManager Client { get; set; }
        private string Key { get; set; }
        private Timer Timer { get; set; }
        private NetDataWriter Writer { get; set; } = new NetDataWriter();
        private SymmetricAlgorithm Alg { get; set; } = DES.Create();
        private ICryptoTransform Encryptor { get; set; }
        private ICryptoTransform Decryptor { get; set; }

        public string ChatText
        {
            get { return _chatText; }
            set
            {
                if (_chatText == value)
                    return;
                _chatText = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ChatText"));
            }
        }

        public string InputText
        {

            get { return _inputText; }
            set
            {
                if (_inputText == value)
                    return;
                _inputText = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("InputText"));
            }
        }

        private string _encrypt(string str)
        {
            var buf = _convert(str);
            return Convert.ToBase64String(Encryptor.TransformFinalBlock(buf, 0, buf.Length));
        }

        private string _decrypt(string str)
        {
            var buf = Convert.FromBase64String(str);
            return Encoding.UTF8.GetString(Decryptor.TransformFinalBlock(buf, 0, buf.Length));
        }

        private bool _inpEnabled = false;

        public bool InputEnabled
        {
            get { return _inpEnabled; }
            set
            {
                if (_inpEnabled == value)
                    return;
                _inpEnabled = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("InputEnabled"));
            }
        }

        private byte[] _convert(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }

        public ChatWindow(string key)
        {
            Key = key;
            Encryptor = Alg.CreateEncryptor(_convert(Key), _convert(Key));
            Decryptor = Alg.CreateDecryptor(_convert(Key), _convert(Key));
            Client = new NetManager(Listener, "SecureChat");
            Client.Start();
            Listener.PeerConnectedEvent += Listener_PeerConnectedEvent;
            Listener.NetworkErrorEvent += Listener_NetworkErrorEvent;
            Listener.PeerDisconnectedEvent += Listener_PeerDisconnectedEvent;
            Listener.NetworkReceiveEvent += Listener_NetworkReceiveEvent;
            Client.MaxConnectAttempts = 2;
            Timer = new Timer(100);
            Timer.Elapsed += (o, s) => { Client.PollEvents(); };
            Timer.AutoReset = true;
            Timer.Enabled = true;
            _writeLine("Trying to connect to server...");
            Client.Connect("headmower.com", 1337);
        }

        private void Listener_NetworkReceiveEvent(NetPeer peer, LiteNetLib.Utils.NetDataReader reader)
        {
            var message = _decrypt(reader.GetString());
            _writeLine($"{DateTime.Now.ToLocalTime()}::{message}");
        }

        private void Listener_PeerDisconnectedEvent(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            _writeLine($"You have been disconnected due a reason: {disconnectInfo.Reason}.");
            InputEnabled = false;
        }

        private void Listener_NetworkErrorEvent(NetEndPoint endPoint, int socketErrorCode)
        {
            _writeLine($"Error with errorcode:{socketErrorCode}.");
        }

        private void _writeLine(string line)
        {
            ChatText += line + "\r\n";
        }

        private void Listener_PeerConnectedEvent(NetPeer peer)
        {
            _writeLine($"You successfully connected!");
            InputEnabled = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _sendMessage();
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                _sendMessage();
                e.Handled = true;
            }
        }

        private void _sendMessage()
        {
            Writer.Put(_encrypt(_inputText));
            _inputText = "";
            Client.GetFirstPeer().Send(Writer, SendOptions.ReliableOrdered);
            Writer.Reset();
        }
    }
}